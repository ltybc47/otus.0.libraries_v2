﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibFromEducation.Classes
{
  public class User
  {
    public string  Guid { get; set; }
    public string Name { get; set; }
    public User()
    {
      Guid = System.Guid.NewGuid().ToString();
    }
    public override string ToString()
    {
      return Name + " " + Guid.ToString();
    }
  }
}
