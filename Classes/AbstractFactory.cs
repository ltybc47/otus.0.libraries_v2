﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWorks2
{
  public interface IApiClient
  {
    void SendRequest();
  }

  public class ApiClientFactory
  {
    public IApiClient Create(string url)
    {
      if (url.StartsWith("ws://"))
      {
        return new WebsoketApiClient();
      } 
      else
      {
        return new HttpApiClient();
      }
    }
  }
  class WebsoketApiClient : IApiClient
  {
    public void SendRequest()
    {
      Console.WriteLine("Web");
    }
  }
  
  class HttpApiClient : IApiClient
  {
    public void SendRequest()
    {
      Console.WriteLine("http");
    }
  }

  class AbstractFactory
  {

  }
}
