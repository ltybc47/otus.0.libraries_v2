﻿using System.IO;
using System.Collections.Generic;
using LibFromEducation.Interfaces;
using System.Linq;
using LibFromEducation.Serialization;
using System;
using System.Text;

namespace LibFromEducation.Classes
{
  public class FileStorage<T> : IStorage<T>, IDisposable
  {
    private string _fileName;
    public List<T> elements = new List<T>();
    ISerializerLib _Serializer;

    public FileStorage(string fileName, ISerializerLib Serializer)
    {
      _fileName = fileName;
      _Serializer = Serializer;
      elements = new List<T>();
      elements = GetAll().ToList();
    }
    public IEnumerable<T> GetAll()
    {
      if (!File.Exists(_fileName)) File.Create(_fileName);
      IEnumerable<T> users = _Serializer.Deserialize<T>(File.ReadAllText(_fileName));

      return users.ToList();
    }

    public void Add(T obj)
    {
      using( var sw = new StreamWriter(_fileName, true, Encoding.Default))
      {
        sw.Write(_Serializer.Serialize(obj));
      }
      elements.Add(obj);
    }

    public T Get()
    {
      throw new System.NotImplementedException();
    }

    public T GetFirstByProperty(string propertyName, object Value)
    {
      if (elements.Count() > 0)
        foreach (T t in elements)
        {
          if (t.GetType().GetProperty(propertyName).GetValue(t).Equals(Value))
          {
            return t;
          }
        }
      return default;
      //throw new System.NullReferenceException();    // Корректно ли это? если не нашли
    }

    public void Dispose()
    {
      _Serializer.Dispose();
    }
  }
}