﻿using CsvHelper;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using LibFromEducation.Classes;
using Newtonsoft.Json;

namespace LibFromEducation.Serialization
{
  public class TestCsvSerializer : ISerializerLib
  {
    public TestCsvSerializer()
    {
    }
    public IEnumerable<T> DeserializeFile<T>(string fileName)
    {
      var csv = new CsvReader(new StreamReader(fileName), CultureInfo.InvariantCulture);
      csv.Configuration.HasHeaderRecord = false;
      var records = csv.GetRecords<T>();
      return records.ToList();
      //List<T> users = records.ToList();
      //Console.WriteLine(string.Join("\n", users.Select(x => ((x as User).Name + " " + (x as User).Guid))));
      //Console.WriteLine(JsonConvert.SerializeObject(records));
    }

    /// <summary>
    /// save List objects
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public bool SaveToFile<T>(List<T> obj, string fileName)
    {
      try
      {
        using (var streamWriter = new StreamWriter(fileName))
        using (var csv = new CsvWriter(streamWriter, CultureInfo.InvariantCulture))
        {
          csv.WriteRecords(obj);
          return true;
        }
      }
      catch (Exception)
      {
        //todo make Log method
        return false;
      }

    }
    /// <summary>
    /// save One object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public bool SaveToFile<T>(T obj, string fileName)
    {
      try
      {
        using (var streamWriter = new StreamWriter(fileName))
        using (var csv = new CsvWriter(streamWriter, CultureInfo.InvariantCulture))
        {
          csv.WriteRecord(obj);
          return true;
        }
      }
      catch (Exception)
      {
        //todo make Log method
        return false;
      }

    }

    public IEnumerable<T> Deserialize<T>(string v)
    {
      IEnumerable<T> records;
      //using (TextReader reader = new StringReader(v))
      //using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
      //{
      //  csv.Configuration.HasHeaderRecord = false;
      //  records =  csv.GetRecords<T>();
      //  Console.WriteLine(JsonConvert.SerializeObject(records));
      //  return records;
      //}
      TextReader reader = new StringReader(v);
      var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
      csv.Configuration.HasHeaderRecord = false;
      return csv.GetRecords<T>().ToList();
      //using (var reader = new StringReader(v))
      //using (var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture))
      //{
      //  csvReader.Configuration.HasHeaderRecord = false;
      //  csvReader.Read();
      //  List<T> records = new List<T>();
      //  records.Add(csvReader.GetRecord<T>());
      //  Console.WriteLine("records.Count()" + records.Count());
      //  //Console.WriteLine(csvReader.GetRecord<T>().GetType().FullName);
      //  //Console.WriteLine("records.First().GetType()" + records.ToArray()[1].GetType());
      //  return records;
      //}
      //using (var reader = new StringReader(v))
      //using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
      //{
      //  csv.Read();
      //  csv.Configuration.HasHeaderRecord = false;
      //  csv.Configuration.PrepareHeaderForMatch = (header, index) => header.ToLower();
      //  IEnumerable<T> records = csv.GetRecords<T>();
      //  //Console.WriteLine(string.Join(
      //  //  "\n",
      //  //  records.Select(r => r.GetType().GetProperties().Select(x => x.Name + " " + x.GetValue(r) + "\n").ToList())));
      //  return records;
      //}
    }
    public string Serialize<T>(List<T> items)
    {
      //using (var writer = new StreamWriter("storage.csv", true))
      using (var stream = new MemoryStream())
      using (var reader = new StreamReader(stream))
      using (var writer = new StreamWriter(stream))
      using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
      {
        csv.Configuration.HasHeaderRecord = false;
        csv.WriteRecords(items);
        writer.Flush();
        stream.Position = 0;
        return reader.ReadToEnd();
      }

      //  using (var memStream = new MemoryStream())
      //  using (var streamWriter = new StreamWriter(memStream))
      //  using (var csv = new CsvWriter(streamWriter, CultureInfo.InvariantCulture))
      //  {
      //    csv.WriteRecord(item);
      //    csv.Context.
      //    streamWriter.WriteLine(csv.Context.ToString());
      //    streamWriter.Flush();
      //    memStream.Seek(0, SeekOrigin.Begin);

      //    return string.Join(",",csv.Context.Record);
      //  }
    }

    public string Serialize<T>(T item)
    {
      using (var stream = new MemoryStream())
      using (var reader = new StreamReader(stream))
      using (var writer = new StreamWriter(stream))
      using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
      {
        csv.Configuration.HasHeaderRecord = false;
        csv.WriteRecords(new List<T>() { item });
        writer.Flush();
        stream.Position = 0;
        return reader.ReadToEnd();
      }
    }

    public void Dispose()
    {
    }
  }
}
