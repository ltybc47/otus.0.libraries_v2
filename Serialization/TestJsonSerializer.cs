﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LibFromEducation.Serialization
{
  public class TestJsonSerializer : ISerializerLib
  {
    public T Deserialize<T>(string v)
    {
      return JsonConvert.DeserializeObject<T>(v);
    }

    public IEnumerable<T> DeserializeList<T>(string v)
    {
      return JsonConvert.DeserializeObject<List<T>>(v);
    }

    public void Dispose()
    {
      throw new NotImplementedException();
    }

    public string Serialize<T>(T item)
    {
      return JsonConvert.SerializeObject(item);
    }

    public string Serialize<T>(List<T> item)
    {
      throw new NotImplementedException();
    }

    IEnumerable<T> ISerializerLib.Deserialize<T>(string s)
    {
      return DeserializeList<T>(s);
    }
  }
}
