﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace LibFromEducation.Serialization
{
  public interface ISerializerLib : IDisposable
  {
    string Serialize<T>(T item);

    string Serialize<T>(List<T> item);

    IEnumerable<T> Deserialize<T>(string s);
  }

  //class OtusXmlSerializer : ISerializerLib
  //{
  //  public string Serialize<T>(T item)
  //  {
  //    using var sw = new StringWriter();
  //    using var xw = XmlWriter.Create(sw);

  //    var serializer = new XmlSerializer(typeof(T));
  //    serializer.Serialize(xw, item);

  //    return sw.ToString();
  //  }
  //}
}
