﻿using LibFromEducation.Classes;
using System.Collections.Generic;

namespace LibFromEducation.Interfaces
{
  public interface IStorage<T>
  {
    void Add(T obj);
    T Get();
    IEnumerable<T> GetAll();
    /// <summary>
    /// Возвращает первый элемент который найдет по значению свойства
    /// </summary>
    /// <param name="propertyName"></param>
    /// <param name="Value"></param>
    /// <returns></returns>
    T GetFirstByProperty(string propertyName, object Value);
  }
}