﻿using System;

namespace LibFromEducation.Interfaces
{
  interface IMessage
  {
    public int IdFromUser { get; set; }
    public int IdToUser { get; set; }
    public string Msg { get; set; }
    public Exception Exception { get; set; }
    public DateTime DateTime { get; set; }
    public string TypeFormat { get; set; }
  }
}
